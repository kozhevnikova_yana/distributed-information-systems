package ru.nsu.fit.hello_world;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Log4j2
public class HelloWorldApplication {
    private static final Logger rootLogger = LogManager.getRootLogger();
    public static void main(String[] args) {
        rootLogger.info("hello world");
    }
}
