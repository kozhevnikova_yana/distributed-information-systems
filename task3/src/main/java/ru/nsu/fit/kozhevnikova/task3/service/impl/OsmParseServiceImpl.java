package ru.nsu.fit.kozhevnikova.task3.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.fit.kozhevnikova.generated.Node;
import ru.nsu.fit.kozhevnikova.task3.Constants;
import xml.StaxStreamProcessor;
import ru.nsu.fit.kozhevnikova.task3.model.NodeDao;
import ru.nsu.fit.kozhevnikova.task3.repository.NodeRepository;
import ru.nsu.fit.kozhevnikova.task3.service.OsmParseService;

import javax.annotation.PostConstruct;

@Service
public class OsmParseServiceImpl implements OsmParseService {
    private static final int MAX_NODES = 10000;
    @Autowired
    private NodeRepository nodeRepository;

    int nodesCount = 0;
    @PostConstruct
    public void parse() {
        try(StaxStreamProcessor processor = new StaxStreamProcessor(Constants.FILE_PATH)){
            Node node = processor.getNextNode();
            while (node != null) {
                NodeDao nodeDao = new NodeDao(node);
                nodeRepository.save(nodeDao);
                nodesCount++;
                if (nodesCount >= MAX_NODES)
                    break;
                node = processor.getNextNode();
            }
        } catch (Exception ignored){
        }
    }
}