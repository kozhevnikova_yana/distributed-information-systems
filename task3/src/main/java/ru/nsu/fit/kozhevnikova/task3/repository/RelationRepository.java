package ru.nsu.fit.kozhevnikova.task3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.kozhevnikova.task3.model.RelationDao;

import java.math.BigInteger;

public interface RelationRepository extends JpaRepository<RelationDao, BigInteger> {
}