package ru.nsu.fit.kozhevnikova.task3.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.kozhevnikova.generated.Relation;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "RELATION")
public class RelationDao {
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "relation", cascade = CascadeType.ALL)
    private List<MemberDao> member;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "relationDao", cascade = CascadeType.ALL)
    private List<RelationTagDao> tag;

    @Id
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "_user")
    private String user;

    @Column(name = "uid")
    private BigInteger uid;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "version")
    private BigInteger version;

    @Column(name = "changeset")
    private BigInteger changeset;

    @Column(name = "_timestamp")
    private Timestamp timestamp;

}
