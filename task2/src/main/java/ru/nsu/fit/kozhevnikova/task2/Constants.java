package ru.nsu.fit.kozhevnikova.task2;

public class Constants {
    public static final String FILE_PATH = "src/main/resources/RU-NVS.osm.bz2";
    public static final String NAMESPACE_URL = "http://openstreetmap.org/osm/0.6";
    public static final String NODE = "node";
}
