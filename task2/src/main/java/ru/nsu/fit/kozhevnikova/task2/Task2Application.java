package ru.nsu.fit.kozhevnikova.task2;


public class Task2Application {
    public static void main(String[] args){
        try {
            TimeMeter timeMeasure = new TimeMeter();
            timeMeasure.measureQueryTime();
            timeMeasure.measurePrepareStatement();
            timeMeasure.measureBatchTime();
        } catch (Exception err){
            System.out.println(err.getMessage());
        }
    }
}
