package ru.nsu.fit.kozhevnikova.task2.repostitory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.kozhevnikova.task2.config.DbManager;
import ru.nsu.fit.kozhevnikova.task2.dao.TagDao;

import java.math.BigInteger;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class TagRepository {
    static final Logger logger = LogManager.getLogger(TagRepository.class.getName());

    protected PreparedStatement insertStatement;
    protected PreparedStatement batchInsertStatement;
    private PreparedStatement getByIdStatement;
    protected int batchSize;

    protected Connection connection;

    public TagRepository() throws SQLException, ClassNotFoundException {
        connection = DbManager.getConnection();
        insertStatement = connection.prepareCall("INSERT INTO NODE_TAG (k, v, nodeId) VALUES (?, ?, ?)");
        getByIdStatement = connection.prepareCall("SELECT * FROM NODE_TAG WHERE NODEID = ?");
        batchInsertStatement = connection.prepareCall("INSERT INTO NODE_TAG (k, v, nodeId) VALUES (?, ?, ?)");
    }

    private void prepareStatement(TagDao o, PreparedStatement statement) throws SQLException {
        if (o.getK() != null)
            statement.setString(1, o.getK());
        else
            statement.setNull(1, Types.NULL);

        if (o.getV() != null)
            statement.setString(2, o.getV());
        else
            statement.setNull(2, Types.NULL);

        if (o.getRelatedId() != null)
            statement.setLong(3, o.getRelatedId().longValue());
        else
            statement.setNull(3, Types.NULL);
    }

    public void saveWithPreparedStatement(TagDao o) {
        try {
            prepareStatement(o, insertStatement);
            insertStatement.execute();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    public void saveWithExecuteQuery(TagDao o) {
        try {
            connection.createStatement()
                    .execute(String.format("INSERT INTO NODE_TAG (k, v, nodeId) VALUES ('%s', '%s', %s)", o.getK(), o.getV(), o.getRelatedId()));
        } catch (SQLException | NullPointerException e) {
            logger.error(e.getMessage());
        }
    }

    public void saveWithBatch(TagDao o) {
        try {
            prepareStatement(o, batchInsertStatement);
            batchInsertStatement.addBatch();
            batchSize++;
            if (batchSize > 1000)
                flushBatch();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }
    public void flushBatch(){
        try {
            batchInsertStatement.executeBatch();
            batchSize = 0;
            batchInsertStatement.clearBatch();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
    public List<TagDao> getById(BigInteger id) {
        if(id == null)
            return null;

        try {
            getByIdStatement.setLong(1, id.longValue());
            ResultSet result = getByIdStatement.executeQuery();

            List<TagDao> tags = new ArrayList<>();

            while(result.next())
                tags.add(TagDao.extractFromResultSet(result));

            return tags;
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return null;
    }
}