package ru.nsu.fit.kozhevnikova.task2.xml;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import ru.nsu.fit.kozhevnikova.generated.Node;
import ru.nsu.fit.kozhevnikova.task2.Constants;
import ru.nsu.fit.kozhevnikova.task2.xml.XsiTypeReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashMap;

import static ru.nsu.fit.kozhevnikova.task2.Constants.NODE;


@Log4j2
public class StaxStreamProcessor implements AutoCloseable {
    private final XMLStreamReader reader;
    private Unmarshaller nodeUnmarshaller;
    public StaxStreamProcessor(String filename) throws XMLStreamException, IOException, JAXBException {
        BZip2CompressorInputStream in = new BZip2CompressorInputStream(new FileInputStream(filename));
        XMLInputFactory FACTORY = XMLInputFactory.newInstance();
        XMLStreamReader defaultReader = FACTORY.createXMLStreamReader(in);
        reader = new XsiTypeReader(defaultReader, Constants.NAMESPACE_URL);
        JAXBContext jc = JAXBContext.newInstance(Node.class);
        nodeUnmarshaller = jc.createUnmarshaller();
    }

    @Override
    public void close() {
        if (reader != null) {
            try {
                reader.close();
            } catch (XMLStreamException ignored) {
            }
        }
    }

    public boolean readerHasNext() throws XMLStreamException {
        return reader.hasNext();
    }

    public int getEventNext() throws XMLStreamException {
        return reader.next();
    }
    public Node getNextNode(){
      try {
          while (readerHasNext()) {
              int event = getEventNext();
              if (event == XMLStreamReader.START_ELEMENT && reader.getLocalName().equals(NODE)) {
                  return nodeUnmarshaller.unmarshal(reader, Node.class).getValue();
              }
          }
      } catch (XMLStreamException | JAXBException e){
          //LOGGER.error(e.getMessage());
      }
      return null;
    }
    public void findAllElementsWithAttribute(LinkedHashMap<String, Integer> items, int event, String element, String attributeName) throws JAXBException {
        if (event == XMLEvent.START_ELEMENT && element.equals(reader.getLocalName())){
            //Node item = nodeUnmarshaller.unmarshal(reader, Node.class).getValue();
            //items.put(item, items.get(item) + 1);
            int attributesAmount = reader.getAttributeCount();
            for (int i = 0; i < attributesAmount; ++i) {
                if (attributeName.equals(reader.getAttributeLocalName(i))) {
                    String item = reader.getAttributeValue(i);
                    if (items.containsKey(item)) {
                        items.put(item, items.get(item) + 1);
                    } else {
                        items.put(item, 1);
                    }
                }
            }
        }
    }
}