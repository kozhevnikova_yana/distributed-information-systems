package ru.nsu.fit.kozhevnikova.task3.service;

import ru.nsu.fit.kozhevnikova.task3.model.RelationDao;

import java.math.BigInteger;

public interface RelationService {
    RelationDao create(RelationDao relationDao);
    RelationDao read(BigInteger id);
    RelationDao update(BigInteger id, RelationDao relationDao);
    void delete(BigInteger id);
}
