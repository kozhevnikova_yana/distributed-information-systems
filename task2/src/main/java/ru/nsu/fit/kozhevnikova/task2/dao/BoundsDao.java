package ru.nsu.fit.kozhevnikova.task2.dao;

import lombok.Getter;
import ru.nsu.fit.kozhevnikova.generated.Bounds;
import java.math.BigInteger;

@Getter
public class BoundsDao {
    private BigInteger id;
    private Double minlat;
    private Double minlon;
    private Double maxlat;
    private Double maxlon;

    public BoundsDao(Bounds bounds, BigInteger id) {
        this.id = id;
        this.minlat = bounds.getMinlat();
        this.minlon = bounds.getMinlon();
        this.maxlat = bounds.getMaxlat();
        this.maxlon = bounds.getMaxlon();
    }
}
