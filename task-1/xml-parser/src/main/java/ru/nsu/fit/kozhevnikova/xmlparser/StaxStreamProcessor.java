package ru.nsu.fit.kozhevnikova.xmlparser;

import lombok.extern.log4j.Log4j2;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;
import java.util.LinkedHashMap;

@Log4j2
public class StaxStreamProcessor implements AutoCloseable {
    private final XMLStreamReader reader;

    public StaxStreamProcessor(InputStream is) throws XMLStreamException {
        XMLInputFactory FACTORY = XMLInputFactory.newInstance();
        FACTORY.setProperty(XMLInputFactory.IS_COALESCING, true);
        reader = FACTORY.createXMLStreamReader(is);
    }

    @Override
    public void close() {
        if (reader != null) {
            try {
gi                reader.close();
            } catch (XMLStreamException ignored) {
            }
        }
    }

    public boolean readerHasNext() throws XMLStreamException {
        return reader.hasNext();
    }

    public int getEventNext() throws XMLStreamException {
        return reader.next();
    }
    public void findAllElementsWithAttribute(LinkedHashMap<String, Integer> items, int event, String element, String attributeName){
        if (event == XMLEvent.START_ELEMENT && element.equals(reader.getLocalName())){
            int attributesAmount = reader.getAttributeCount();
            for (int i = 0; i < attributesAmount; ++i) {
                if (attributeName.equals(reader.getAttributeLocalName(i))) {
                    String item = reader.getAttributeValue(i);
                    if (items.containsKey(item)) {
                        items.put(item, items.get(item) + 1);
                    } else {
                        items.put(item, 1);
                    }
                }
            }
        }
    }
}