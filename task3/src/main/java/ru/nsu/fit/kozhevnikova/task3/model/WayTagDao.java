package ru.nsu.fit.kozhevnikova.task3.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.kozhevnikova.generated.Tag;

import javax.persistence.*;
import java.math.BigInteger;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "WAY_TAG")
public class WayTagDao {
    @JsonIgnore
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private BigInteger id;

    @Column(name = "k")
    private String k;

    @Column(name = "v")
    private String v;

    @JsonIgnore
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "WAYID")
    private WayDao way;

    public WayTagDao(Tag tag, WayDao way) {
        this.k = tag.getK();
        this.v = tag.getV();
        this.way = way;
    }
}
