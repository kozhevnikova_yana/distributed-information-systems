package ru.nsu.fit.kozhevnikova.task3.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.kozhevnikova.generated.Tag;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;

@Getter
@Setter
@NoArgsConstructor
public class TagDao {
    private String k;

    private String v;

    private BigInteger relatedId;
}
