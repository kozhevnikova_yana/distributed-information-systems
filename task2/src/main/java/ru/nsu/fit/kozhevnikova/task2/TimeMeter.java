package ru.nsu.fit.kozhevnikova.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.kozhevnikova.generated.Node;
import ru.nsu.fit.kozhevnikova.task2.dao.NodeDao;
import ru.nsu.fit.kozhevnikova.task2.config.DbManager;
import ru.nsu.fit.kozhevnikova.task2.repostitory.NodeRepository;
import ru.nsu.fit.kozhevnikova.task2.xml.StaxStreamProcessor;

import java.sql.SQLException;

public class TimeMeter {
    private static final Logger logger = LogManager.getLogger(TimeMeter.class.getName());
    private static final int MAX_NODES = 1000000;
    private final NodeRepository nodeRepository;
    public TimeMeter() throws SQLException, ClassNotFoundException {
        nodeRepository = new NodeRepository();
    }
    public void measureQueryTime(){
        DbManager.initDb();
        long totalTime = 0;
        int nodesCount = 0;
        try(StaxStreamProcessor processor = new StaxStreamProcessor(Constants.FILE_PATH)){
            Node node = processor.getNextNode();
            while (node != null) {
                NodeDao nodeDao = new NodeDao(node, null);

                long startTime = System.currentTimeMillis();
                nodeRepository.saveWithExecuteQuery(nodeDao);
                long endTime = System.currentTimeMillis();
                totalTime += endTime - startTime;
                nodesCount++;
                if(nodesCount >= MAX_NODES)
                    break;
                node = processor.getNextNode();
            }
        }catch (Exception e){
            logger.error(e.getLocalizedMessage());
        }
        double executeQueryTime = (double) totalTime / 1000.0;
        logger.info(String.format("EXECUTE QUERY TIME = %s (rows/sec)", MAX_NODES / executeQueryTime));
    }
    public void measurePrepareStatement(){
        DbManager.initDb();
        long totalTime = 0;
        int nodesCount = 0;
        try(StaxStreamProcessor processor = new StaxStreamProcessor(Constants.FILE_PATH)){
            Node node = processor.getNextNode();
            while (node != null) {
                NodeDao nodeDao = new NodeDao(node, null);

                long startTime = System.currentTimeMillis();
                nodeRepository.saveWithPreparedStatement(nodeDao);
                long endTime = System.currentTimeMillis();
                totalTime += endTime - startTime;
                nodesCount++;
                if(nodesCount >= MAX_NODES)
                    break;
                node = processor.getNextNode();
            }
        }catch (Exception e){
            logger.error(e.getLocalizedMessage());
        }
        double preparedTime = (double) totalTime / 1000.0;
        logger.info(String.format("PREPARED STATEMENT TIME = %s (rows/sec)", MAX_NODES / preparedTime));

    }
    public void measureBatchTime(){
        DbManager.initDb();
        long totalTime = 0;
        int nodesCount = 0;
        try(StaxStreamProcessor processor = new StaxStreamProcessor(Constants.FILE_PATH)){
            Node node = processor.getNextNode();
            while (node != null) {
                NodeDao nodeDao = new NodeDao(node, null);

                long startTime = System.currentTimeMillis();
                nodeRepository.saveWithBatch(nodeDao);
                long endTime = System.currentTimeMillis();
                totalTime += endTime - startTime;
                nodesCount++;
                if(nodesCount >= MAX_NODES)
                    break;
                node = processor.getNextNode();
            }
        }catch (Exception e){
            logger.error(e.getLocalizedMessage());
        }
        double batchTime = (double) totalTime / 1000.0;
        long startTime = System.currentTimeMillis();
        nodeRepository.flushBatch();
        long endTime = System.currentTimeMillis();

        batchTime += (double) (endTime - startTime) / 1000.0;
        logger.info(String.format("BATCH TIME = %s (rows/sec)",  MAX_NODES / batchTime));
    }
}