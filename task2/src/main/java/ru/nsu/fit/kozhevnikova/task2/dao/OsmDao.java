package ru.nsu.fit.kozhevnikova.task2.dao;


import lombok.Getter;
import ru.nsu.fit.kozhevnikova.generated.Osm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Getter
public class OsmDao {
    private BoundsDao bounds;

    private List<NodeDao> node;

    private List<WayDao> way;

    private List<RelationDao> relation;

    private Float version;

    private String generator;

    private BigInteger id;


    public OsmDao(Osm osm, BigInteger boundsId, BigInteger id) {
        this.version = osm.getVersion();
        this.generator = osm.getGenerator();
        this.bounds = new BoundsDao(osm.getBounds(), boundsId);
        this.id = id;

        this.relation = new ArrayList<>();
        if (osm.getRelation() != null)
            osm.getRelation().forEach(r -> relation.add(new RelationDao(r, id)));

        this.node = new ArrayList<>();
        if (osm.getNode() != null)
            osm.getNode().forEach(n -> node.add(new NodeDao(n, id)));

        this.way = new ArrayList<>();
        if (osm.getWay() != null)
            osm.getWay().forEach(w -> way.add(new WayDao(w, id)));
    }
}