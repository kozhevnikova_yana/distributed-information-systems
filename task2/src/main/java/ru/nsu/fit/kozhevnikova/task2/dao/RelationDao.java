package ru.nsu.fit.kozhevnikova.task2.dao;

import lombok.Getter;
import ru.nsu.fit.kozhevnikova.generated.Relation;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
public class RelationDao {
    private List<MemberDao> member;

    private List<TagDao> tag;

    private BigInteger id;

    private String user;

    private BigInteger uid;

    private Boolean visible;

    private BigInteger version;

    private BigInteger changeset;

    private Timestamp timestamp;

    private BigInteger osmId;
    public RelationDao(Relation relation, BigInteger osmId) {
        this.id = relation.getId();
        this.timestamp = new Timestamp(relation.getTimestamp().getMillisecond());
        this.changeset = relation.getChangeset();
        this.version = relation.getVersion();
        this.visible = relation.isVisible();
        this.uid = relation.getUid();
        this.user= relation.getUser();
        this.osmId = osmId;

        this.tag = new ArrayList<>();
        if(relation.getTag() != null)
            relation.getTag().forEach(t -> tag.add(new TagDao(t, id)));

        this.member = new ArrayList<>();
        if(relation.getMember() != null)
            relation.getMember().forEach(m -> member.add(new MemberDao(m, id)));
    }
}
