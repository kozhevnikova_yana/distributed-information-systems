package ru.nsu.fit.kozhevnikova.task2.repostitory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.kozhevnikova.task2.config.DbManager;
import ru.nsu.fit.kozhevnikova.task2.dao.NodeDao;
import ru.nsu.fit.kozhevnikova.task2.dao.TagDao;

import java.sql.*;

public class NodeRepository {
    protected static final Logger logger = LogManager.getLogger(NodeRepository.class.getName());

    protected PreparedStatement insertStatement;
    protected PreparedStatement batchInsertStatement;

    protected int batchSize;

    protected Connection connection;

    private final TagRepository tagRepository;

    public NodeRepository() throws SQLException, ClassNotFoundException {
        connection = DbManager.getConnection();
        insertStatement = connection.prepareCall("INSERT INTO NODE (id, lat, lon, _user, uid, visible, version, changeset, _timestamp) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        batchInsertStatement = connection.prepareCall("INSERT INTO NODE (id, lat, lon, _user, uid, visible, version, changeset, _timestamp) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        tagRepository = new TagRepository();
    }

    private void prepareStatement(NodeDao o, PreparedStatement statement) throws SQLException {
        if (o.getId() != null)
            statement.setLong(1, o.getId().longValue());
        else
            statement.setNull(1, Types.NULL);

        if (o.getLat() != null)
            statement.setDouble(2, o.getLat());
        else
            statement.setNull(2, Types.NULL);

        if (o.getLon() != null)
            statement.setDouble(3, o.getLon());
        else
            statement.setNull(3, Types.NULL);

        if (o.getUser() != null)
            statement.setString(4, o.getUser());
        else
            statement.setNull(4, Types.NULL);

        if (o.getUid() != null)
            statement.setLong(5, o.getUid().longValue());
        else
            statement.setNull(5, Types.NULL);

        if (o.getVisible() != null)
            statement.setBoolean(6, o.getVisible());
        else
            statement.setNull(6, Types.NULL);

        if (o.getVersion() != null)
            statement.setLong(7, o.getVersion().longValue());
        else
            statement.setNull(7, Types.NULL);

        if (o.getChangeset() != null)
            statement.setLong(8, o.getChangeset().longValue());
        else
            statement.setNull(8, Types.NULL);

        if (o.getTimestamp() != null)
            statement.setTimestamp(9, o.getTimestamp());
        else
            statement.setNull(9, Types.NULL);
    }

    public void saveWithExecuteQuery(NodeDao o) {
        try {
            connection.createStatement()
                    .execute(String.format(
                            "INSERT INTO NODE (id, lat, lon, _user, uid, visible, version, changeset, _timestamp) VALUES (%s, %s, %s, '%s', %s, %s, %s, %s, '%s')",
                            o.getId(), o.getLat(), o.getLon(), o.getUser(), o.getUid(), o.getVisible(), o.getVersion(), o.getChangeset(), o.getTimestamp())
                    );

            for (TagDao tag : o.getTags()) {
                tagRepository.saveWithExecuteQuery(tag);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    public void saveWithPreparedStatement(NodeDao o) {
        try {
            prepareStatement(o, insertStatement);
            insertStatement.execute();

            for (TagDao tag : o.getTags()) {
                tagRepository.saveWithPreparedStatement(tag);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    public void saveWithBatch(NodeDao o) {
        try {
            prepareStatement(o, batchInsertStatement);
            batchInsertStatement.addBatch();
            batchSize++;

            for (TagDao tag : o.getTags()) {
                tagRepository.saveWithBatch(tag);
            }
            if (batchSize > 1000) {
                flushBatch();
            }
        } catch (SQLException e) {
            logger.error(e.getNextException());
        }
    }
    public void flushBatch() {
        try {
            batchInsertStatement.executeBatch();
            batchSize = 0;
            batchInsertStatement.clearBatch();
            tagRepository.flushBatch();
        } catch (SQLException e) {
            logger.error(e.getNextException());
        }
    }

}