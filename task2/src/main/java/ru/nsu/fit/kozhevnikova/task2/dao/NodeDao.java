package ru.nsu.fit.kozhevnikova.task2.dao;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.kozhevnikova.generated.Node;
import ru.nsu.fit.kozhevnikova.task2.repostitory.TagRepository;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class NodeDao {
    private BigInteger id;

    private Double lat;

    private Double lon;

    private String user;

    private BigInteger uid;

    private Boolean visible;

    private BigInteger version;

    private BigInteger changeset;

    private Timestamp timestamp;

    private List<TagDao> tags;

    private BigInteger osmId;

    public NodeDao(Node node, BigInteger osmId) {
        this.id = node.getId();
        this.lat = node.getLat();
        this.lon = node.getLon();
        this.user = node.getUser();
        this.uid = node.getUid();
        this.visible = node.isVisible();
        this.version = node.getVersion();
        this.changeset = node.getChangeset();
        this.osmId = osmId;
        this.timestamp = new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis());

        this.tags = new ArrayList<>();

        if (node.getTag() != null)
            node.getTag().forEach(tag -> tags.add(new TagDao(tag, id)));
    }
    public static NodeDao extractFromResultSet(ResultSet result) throws SQLException, ClassNotFoundException {
        NodeDao nodeDao = new NodeDao();

        nodeDao.setId(BigInteger.valueOf(result.getLong(1)));
        nodeDao.setLat(result.getDouble(2));
        nodeDao.setLon(result.getDouble(3));
        nodeDao.setUser(result.getString(4));
        nodeDao.setUid(BigInteger.valueOf(result.getLong(5)));
        nodeDao.setVisible(result.getBoolean(6));
        nodeDao.setVersion(BigInteger.valueOf(result.getLong(7)));
        nodeDao.setChangeset(BigInteger.valueOf(result.getLong(8)));
        nodeDao.setTimestamp(result.getTimestamp(9));

        TagRepository tagRepository = new TagRepository();
        nodeDao.setTags(tagRepository.getById(nodeDao.getId()));

        return nodeDao;
    }
}
