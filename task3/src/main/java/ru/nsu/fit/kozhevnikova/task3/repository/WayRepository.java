package ru.nsu.fit.kozhevnikova.task3.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.kozhevnikova.task3.model.WayDao;

import java.math.BigInteger;

public interface WayRepository extends JpaRepository<WayDao, BigInteger> {
}