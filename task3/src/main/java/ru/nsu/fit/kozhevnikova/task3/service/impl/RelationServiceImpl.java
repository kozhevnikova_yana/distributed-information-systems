package ru.nsu.fit.kozhevnikova.task3.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.fit.kozhevnikova.task3.model.RelationDao;
import ru.nsu.fit.kozhevnikova.task3.repository.RelationRepository;
import ru.nsu.fit.kozhevnikova.task3.service.RelationService;

import java.math.BigInteger;

@Service
public class RelationServiceImpl implements RelationService {
    @Autowired
    private RelationRepository relationRepository;

    public RelationDao create(RelationDao relationDao){
        return relationRepository.save(relationDao);
    }
    public RelationDao read(BigInteger id){
        return relationRepository.getOne(id);
    }
    public RelationDao update(BigInteger id, RelationDao node){
        RelationDao relationDao = relationRepository.findById(id).orElseThrow(NullPointerException::new);
        node.setId(relationDao.getId());
        return relationRepository.save(node);
    }
    public void delete(BigInteger id){
        RelationDao relationDao = relationRepository.getOne(id);
        relationRepository.delete(relationDao);
    }
}
