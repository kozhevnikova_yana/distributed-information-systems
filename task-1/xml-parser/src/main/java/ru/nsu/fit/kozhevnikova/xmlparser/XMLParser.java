package ru.nsu.fit.kozhevnikova.xmlparser;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
public class  XMLParser {
    private static final Logger rootLogger = LogManager.getRootLogger();
    public void run(String path) {
        try (StaxStreamProcessor processor = new StaxStreamProcessor(Files.newInputStream(Paths.get(path)))) {
            LinkedHashMap<String, Integer> userEdits = new LinkedHashMap<>();
            LinkedHashMap<String, Integer> keyEntries = new LinkedHashMap<>();
            rootLogger.info("start parse xml");
            while (processor.readerHasNext()) {
                int event = processor.getEventNext();
                processor.findAllElementsWithAttribute(userEdits, event, "node", "user");
                processor.findAllElementsWithAttribute(keyEntries, event, "tag", "k");
            }
            userEdits = sortMap(userEdits);
            keyEntries = sortMap(keyEntries);
            printResult(userEdits, keyEntries);
            rootLogger.info("the parsing of the file is completed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private LinkedHashMap<String, Integer> sortMap(LinkedHashMap<String, Integer> map){
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k1, k2) -> k1, LinkedHashMap::new));
    }
    private void printResult(LinkedHashMap<String, Integer> userEdits, LinkedHashMap<String, Integer> keyEntries){
        userEdits.forEach((user, amount) -> rootLogger.info("User: " + user + "; " + "Number of edits: " + amount));
        keyEntries.forEach((key, amount) -> rootLogger.info("Key: " + key + "; " + "Number of occurrences: " + amount));
    }
}
