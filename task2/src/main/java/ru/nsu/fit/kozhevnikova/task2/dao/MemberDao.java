package ru.nsu.fit.kozhevnikova.task2.dao;

import lombok.Data;
import lombok.Getter;
import ru.nsu.fit.kozhevnikova.generated.Member;

import java.math.BigInteger;

@Getter
public class MemberDao {
    private String type;

    private BigInteger ref;

    private String role;

    private BigInteger relationId;

    public MemberDao(Member member, BigInteger relationId) {
        this.type = member.getType();
        this.ref = member.getRef();
        this.role = member.getRole();
        this.relationId = relationId;
    }
}
