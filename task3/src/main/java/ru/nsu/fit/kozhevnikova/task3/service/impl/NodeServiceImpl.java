package ru.nsu.fit.kozhevnikova.task3.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.fit.kozhevnikova.task3.model.NodeDao;
import ru.nsu.fit.kozhevnikova.task3.repository.NodeRepository;
import ru.nsu.fit.kozhevnikova.task3.service.NodeService;

import java.math.BigInteger;
import java.util.Collection;

@Service
public class NodeServiceImpl implements NodeService {
    @Autowired
    private NodeRepository nodeRepository;

    public NodeDao create(NodeDao nodeDao){
       return nodeRepository.save(nodeDao);
    }
    public NodeDao read(BigInteger id){
        return nodeRepository.getNodeDaoById(id);
    }
    public NodeDao update(BigInteger id, NodeDao node){
        NodeDao nodeFromDb = nodeRepository.findById(id).orElseThrow(NullPointerException::new);
        node.setId(nodeFromDb.getId());
        return nodeRepository.save(node);
    }
    public void delete(BigInteger id){
        NodeDao nodeDao = nodeRepository.getNodeDaoById(id);
        nodeRepository.delete(nodeDao);
    }
    public NodeDao[] getNodeInRadius(Float latitude, Float longitude, Float radius){
        Collection<NodeDao> d = nodeRepository.getAllByDistance(latitude, longitude, radius);
        return d.toArray(new NodeDao[0]);
    }
}
