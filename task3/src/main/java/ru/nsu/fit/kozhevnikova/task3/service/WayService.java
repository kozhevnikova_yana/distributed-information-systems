package ru.nsu.fit.kozhevnikova.task3.service;

import ru.nsu.fit.kozhevnikova.task3.model.WayDao;

import java.math.BigInteger;

public interface WayService {
    WayDao create(WayDao wayDao);
    WayDao read(BigInteger id);
    WayDao update(BigInteger id, WayDao wayDao);
    void delete(BigInteger id);
}
