package ru.nsu.fit.kozhevnikova.task3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.nsu.fit.kozhevnikova.task3.model.NodeDao;

import java.math.BigInteger;
import java.util.Collection;

public interface NodeRepository extends JpaRepository<NodeDao, BigInteger> {

    @Query("SELECT n from NodeDao n where n.id = ?1")
    NodeDao getNodeDaoById(BigInteger id);

    @Query(value = "SELECT * FROM node WHERE gc_to_sec(earth_distance(ll_to_earth(?1, ?2), ll_to_earth(node.lat, node.lon))) < ?3 ORDER BY gc_to_sec(earth_distance(ll_to_earth(?1, ?2), ll_to_earth(node.lat, node.lon))) ASC",  nativeQuery = true)
    Collection<NodeDao> getAllByDistance(Float latitude, Float longitude, Float radius);
}