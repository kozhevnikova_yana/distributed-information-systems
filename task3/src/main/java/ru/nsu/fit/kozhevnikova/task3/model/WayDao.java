package ru.nsu.fit.kozhevnikova.task3.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.kozhevnikova.generated.Way;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "WAY")
public class WayDao {
    @Id
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "_user")
    private String user;

    @Column(name = "uid")
    private BigInteger uid;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "changeset")
    private BigInteger changeset;

    @Column(name = "_timestamp")
    private Timestamp timestamp;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "way", cascade = CascadeType.ALL)
    private List<NdDao> ndDaos;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "way", cascade = CascadeType.ALL)
    private List<WayTagDao> tags;

}
