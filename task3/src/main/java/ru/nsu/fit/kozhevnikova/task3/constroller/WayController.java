package ru.nsu.fit.kozhevnikova.task3.constroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.kozhevnikova.task3.model.RelationDao;
import ru.nsu.fit.kozhevnikova.task3.model.WayDao;
import ru.nsu.fit.kozhevnikova.task3.repository.WayRepository;
import ru.nsu.fit.kozhevnikova.task3.service.RelationService;
import ru.nsu.fit.kozhevnikova.task3.service.WayService;

import java.math.BigInteger;

@Controller
@RequestMapping(value = "/way")
public class WayController {
    @Autowired
    private WayService wayService;

    @GetMapping("/{id}")
    ResponseEntity<?> g(@RequestParam(name = "id") BigInteger id){
        WayDao wayDao =  wayService.read(id);
        return new ResponseEntity<>(wayDao, HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<?> createWay(@RequestBody WayDao WayDao){
        WayDao wayDao = wayService.create(WayDao);
        return new ResponseEntity<>(wayDao, HttpStatus.OK);
    }
    @PutMapping("/{id}")
    ResponseEntity<?> updateNode(@PathVariable("id") BigInteger id,
                                 @RequestBody WayDao wayDao){
        WayDao dao = wayService.update(id, wayDao);
        return new ResponseEntity<>(dao, HttpStatus.OK);
    }
    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteNode(@RequestParam(name = "id") BigInteger id) {
        wayService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}