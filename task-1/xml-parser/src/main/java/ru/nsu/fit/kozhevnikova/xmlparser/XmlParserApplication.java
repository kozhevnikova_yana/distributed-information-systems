package ru.nsu.fit.kozhevnikova.xmlparser;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class XmlParserApplication{
    private static final String FILE_PATH = "src/main/resources/RU-NVS.osm";
    public static void main(String[] args){
        XMLParser xmlParser = new XMLParser();
        xmlParser.run(FILE_PATH);
    }
}