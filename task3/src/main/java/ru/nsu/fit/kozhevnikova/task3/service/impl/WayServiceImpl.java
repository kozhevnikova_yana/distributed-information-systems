package ru.nsu.fit.kozhevnikova.task3.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.fit.kozhevnikova.task3.model.WayDao;
import ru.nsu.fit.kozhevnikova.task3.repository.WayRepository;
import ru.nsu.fit.kozhevnikova.task3.service.WayService;

import java.math.BigInteger;

@Service
public class WayServiceImpl implements WayService {
    @Autowired
    private WayRepository wayRepository;
    @Override
    public WayDao create(WayDao wayDao) {
        return wayRepository.save(wayDao);
    }

    @Override
    public WayDao read(BigInteger id) {
        return wayRepository.getOne(id);
    }

    public WayDao update(BigInteger id, WayDao node){
        WayDao wayDao = wayRepository.findById(id).orElseThrow(NullPointerException::new);
        node.setId(wayDao.getId());
        return wayRepository.save(node);
    }
    public void delete(BigInteger id){
        WayDao wayDao = wayRepository.getOne(id);
        wayRepository.delete(wayDao);
    }
}
