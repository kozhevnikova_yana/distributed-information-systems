package ru.nsu.fit.kozhevnikova.task3.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.kozhevnikova.generated.Member;

import javax.persistence.*;
import java.math.BigInteger;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "MEMBER")
public class MemberDao {
    @JsonIgnore
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private BigInteger id;

    @Column(name = "type")
    private String type;

    @Column(name = "ref")
    private BigInteger ref;

    @Column(name = "role")
    private String role;

    @JsonIgnore
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "relation")
    private RelationDao relation;
}
