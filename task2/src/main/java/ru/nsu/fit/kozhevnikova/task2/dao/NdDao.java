package ru.nsu.fit.kozhevnikova.task2.dao;

import lombok.Getter;
import ru.nsu.fit.kozhevnikova.generated.Nd;

import java.math.BigInteger;

@Getter
public class NdDao {
    private BigInteger ref;

    private BigInteger way;

    public NdDao(Nd nd, BigInteger way) {
        this.ref = nd.getRef();
        this.way = way;
    }
}
