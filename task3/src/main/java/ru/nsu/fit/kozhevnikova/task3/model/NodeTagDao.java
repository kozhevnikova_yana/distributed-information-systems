package ru.nsu.fit.kozhevnikova.task3.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.kozhevnikova.generated.Tag;

import javax.persistence.*;
import java.math.BigInteger;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "NODE_TAG")
public class NodeTagDao {
    @JsonIgnore
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private BigInteger id;

    @Column(name = "k")
    private String k;

    @Column(name = "v")
    private String v;

    @JsonIgnore
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "NODEID")
    private NodeDao nodeId;

    public NodeTagDao(Tag tag, NodeDao nodeId) {
        this.k = tag.getK();
        this.v = tag.getV();

        this.nodeId = nodeId;
    }
}
