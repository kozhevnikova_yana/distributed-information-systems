package ru.nsu.fit.kozhevnikova.task3.constroller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.kozhevnikova.task3.model.NodeDao;

import ru.nsu.fit.kozhevnikova.task3.service.NodeService;

import java.math.BigInteger;

@Controller
@RequestMapping(value = "/node")
public class NodeController {
    @Autowired
    private NodeService nodeService;

    @GetMapping("/{id}")
    ResponseEntity<?> getNode(@RequestParam(name = "id") BigInteger id){
        NodeDao node =  nodeService.read(id);
        return new ResponseEntity<>(node, HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<?> createNode(@RequestBody NodeDao nodeDao){
        NodeDao node = nodeService.create(nodeDao);
        return new ResponseEntity<>(node, HttpStatus.OK);
    }
    @PutMapping("/{id}")
    ResponseEntity<?> updateNode(@PathVariable("id") BigInteger id,
                                 @RequestBody NodeDao nodeDao){
        NodeDao node = nodeService.update(id, nodeDao);
        return new ResponseEntity<>(node, HttpStatus.OK);
    }
    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteNode(@RequestParam(name = "id") BigInteger id) {
        nodeService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @GetMapping(value = "/distance")
    @ResponseBody
    public NodeDao[] getNearestNodes(@RequestParam("latitude") Float latitude,
                                     @RequestParam("longitude") Float longitude,
                                     @RequestParam("radius") Float radius) {
        return nodeService.getNodeInRadius(latitude, longitude, radius);
    }
}