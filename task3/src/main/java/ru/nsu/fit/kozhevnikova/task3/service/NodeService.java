package ru.nsu.fit.kozhevnikova.task3.service;

import ru.nsu.fit.kozhevnikova.task3.model.NodeDao;
import java.math.BigInteger;

public interface NodeService {
    NodeDao create(NodeDao nodeDao);
    NodeDao read(BigInteger id);
    NodeDao update(BigInteger id, NodeDao node);
    void delete(BigInteger id);
    NodeDao[] getNodeInRadius(Float latitude, Float longitude, Float radius);
}
