package ru.nsu.fit.kozhevnikova.task2.dao;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.kozhevnikova.generated.Tag;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;

@Getter
@Setter
@NoArgsConstructor
public class TagDao {
    private String k;

    private String v;

    private BigInteger relatedId;

    public TagDao(Tag tag, BigInteger nodeId) {
        this.k = tag.getK();
        this.v = tag.getV();
        this.relatedId = nodeId;
    }
    public static TagDao extractFromResultSet(ResultSet result) throws SQLException {
        TagDao tagDao = new TagDao();

        tagDao.setK(result.getString(1));
        tagDao.setV(result.getString(2));
        tagDao.setRelatedId(BigInteger.valueOf(result.getLong(3)));

        return tagDao;
    }
}
