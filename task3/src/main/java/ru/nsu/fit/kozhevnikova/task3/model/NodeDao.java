package ru.nsu.fit.kozhevnikova.task3.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.kozhevnikova.generated.Node;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "Node")
public class NodeDao {
    @Id
    @Column(name = "id")
    protected BigInteger id;

    @Column(name = "lat")
    protected Double lat;

    @Column(name = "lon")
    protected Double lon;

    @Column(name = "_user")
    protected String user;

    @Column(name = "uid")
    protected BigInteger uid;

    @Column(name = "visible")
    protected Boolean visible;

    @Column(name = "version")
    protected BigInteger version;

    @Column(name = "changeset")
    protected BigInteger changeset;

    @Column(name = "_timestamp")
    protected Timestamp timestamp;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "nodeId", cascade = CascadeType.ALL)
    protected Collection<NodeTagDao> tags;

    public NodeDao(Node node) {
        this.id = node.getId();
        this.lat = node.getLat();
        this.lon = node.getLon();
        this.user = node.getUser();
        this.uid = node.getUid();
        this.visible = node.isVisible();
        this.version = node.getVersion();
        this.changeset = node.getChangeset();
        this.timestamp = new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis());

        tags = new ArrayList<>();
        if(node.getTag() != null)
            node.getTag().forEach(t -> tags.add(new NodeTagDao(t, this)));
    }

}
