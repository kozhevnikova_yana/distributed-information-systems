package ru.nsu.fit.kozhevnikova.task3.constroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.kozhevnikova.task3.model.RelationDao;
import ru.nsu.fit.kozhevnikova.task3.service.RelationService;

import java.math.BigInteger;

@Controller
@RequestMapping(value = "/relation")
public class RelationController {
    @Autowired
    private RelationService relationService;

    @GetMapping("/{id}")
    ResponseEntity<?> g(@RequestParam(name = "id") BigInteger id){
        RelationDao relationDao =  relationService.read(id);
        return new ResponseEntity<>(relationDao, HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<?> createRelation(@RequestBody RelationDao relationDao){
        RelationDao node = relationService.create(relationDao);
        return new ResponseEntity<>(node, HttpStatus.OK);
    }
    @PutMapping("/{id}")
    ResponseEntity<?> updateNode(@PathVariable("id") BigInteger id,
                                 @RequestBody RelationDao relationDao){
        RelationDao relation = relationService.update(id, relationDao);
        return new ResponseEntity<>(relation, HttpStatus.OK);
    }
    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> deleteNode(@RequestParam(name = "id") BigInteger id) {
        relationService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}