package ru.nsu.fit.kozhevnikova.task3.service;

import javax.annotation.PostConstruct;

public interface OsmParseService {
    @PostConstruct
    void parse();
}
