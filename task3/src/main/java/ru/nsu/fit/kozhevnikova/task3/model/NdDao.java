package ru.nsu.fit.kozhevnikova.task3.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.kozhevnikova.generated.Nd;

import javax.persistence.*;
import java.math.BigInteger;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "ND")
public class NdDao {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    @JsonIgnore
    private BigInteger id;

    @Column(name = "ref")
    private BigInteger ref;

    @JsonIgnore
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "WAYID")
    private WayDao way;

    public NdDao(Nd nd, WayDao way) {
        this.ref = nd.getRef();
        this.way = way;
    }
}
